const express = require('express');
const app = express();

const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bodyParser = require('body-parser');

mongoose.connect('mongodb://localhost/expense');

var expenseSchema = mongoose.Schema({
  date : { type: Date, default: Date.now },
  category : { type: String, required : true },
  type : { type: String, required : true,  enum : ['cash','credit'], default: 'cash' },
  amount : { type : Number, required : true }
});
var expenseModel = mongoose.model('expense', expenseSchema);

var categorySchema = mongoose.Schema({
  label : { type: String, unique: true, required : true }
});
var categoryModel = mongoose.model('category', categorySchema);

app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,access_key,method');
	res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  expenseModel.find({},function(err, exp) {
    if (err)
      res.send(err);

    res.json(exp);
  });
  //res.send('Welcome to the coolest api on the earth');
});

app.post('/', function (req, res) {
  var body = req.body;
  //console.log(req);
  if(body == undefined) {
    res.send({error: true, msg: "invalid parameters"});
    return;
  }
  
  if(body.date == undefined || body.date == ""){
    res.send({error: true, msg: "date is invalid"});
    return;
  }
  
  if(body.category == undefined || body.category == ""){
    res.send({error: true, msg: "category is invalid"});
    return;
  }
  
  if(body.type == undefined || body.type == "" || (body.type != "cash" && body.type != "credit")){
    res.send({error: true, msg: "type is invalid"});
    return;
  }

  if(body.amount == undefined || body.amount == 0){

    res.send({error: true, msg: "amount is invalid"});
    return;
  }


  expenseModel.create(body, function (err, small) {
    if (err) 
      res.send({error: true, msg: "Can't add expense", error: err});
      
    res.send({error: false, msg: "Expense added"});
  });
  
})

app.put('/:id', function (req, res) {

  var id = req.params.id;
  var body = req.body;

  if(body == undefined || id == undefined)
    res.send({error: true, msg: "invalid parameters"});
  
  if(body.date == undefined || body.date == "")
    res.send({error: true, msg: "date is invalid"});
  
  if(body.category == undefined || body.category == "")
    res.send({error: true, msg: "category is invalid"});
  
  if(body.type == undefined || (body.type != "cash" && body.type != "credit"))
    res.send({error: true, msg: "type is invalid"});

  if(body.amount == undefined || body.amount == 0)
    res.send({error: true, msg: "amount is invalid"});
    
  
  expenseModel.update({ _id: id }, body, { multi: false }, function(err) {
    if(err) 
      res.send({error: true, msg: "Can't update expense", error: err});
      
    res.send({error: false, msg: "Expense updated"});
  });

})

app.delete('/:id', function (req, res) {
  var id = req.params.id;

  expenseModel.remove({ _id: id }, function(err) {
    if(err) 
      res.send({error: true, msg: "Can't delete expense", error: err});
      
    res.send({error: false, msg: "Expense deleted"});
  });
});

app.get('/expByDate', function(req, res){
  expenseModel.aggregate([{
      $group:
        {
          _id: { day: { $dayOfMonth: "$date"} , month: {$month: "$date"} , year: { $year: "$date" } },
          totalAmount: { $sum: "$amount"  }
        }
    }], function(err,results) {
      if (err)
        res.json(err);
      
      res.json(results);
    });
});

app.get('/expByType', function(req, res){
  expenseModel.aggregate([{
      $group:
        {
          _id: { type: "$type" },
          totalAmount: { $sum: "$amount"  }
        }
    }], function(err,results) {
      if (err) throw err;
      
      res.json(results);
    });
});

app.get('/expByCategory', function(req, res){
  expenseModel.aggregate([{
      $group:
        {
          _id: { category: "$category" },
          totalAmount: { $sum: "$amount"  }
        }
    }], function(err,results) {
      if (err) throw err;
      
      res.json(results);
    });
});

app.options('/:id', function (req, res) {
  res.send({error: false, msg: true});
});
app.options('/', function (req, res) {
  res.send({error: false, msg: true});
});

app.get('/categories', function (req, res) {

  categoryModel.find().distinct('label', function(err, categories) {
    if(err) 
      res.send({error: true, msg: "Can't get categories", error: err});
      
    res.send({error: false, msg: "Success", data: categories});
  });
});

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(3000, () => console.log('Example app listening on port 3000!'));